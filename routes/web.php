<?php

use Monolog\Handler\FirePHPHandler;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\LogoutController;
use App\Http\Controllers\ArticleController;
use App\Http\Controllers\ForgotController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\ResetController;

//Profile access
Route::get('profile/{user}', [UserController::class, 'profile'])->name('user.profile');
Route::resource('articles', ArticleController::class)->except('destroy');
Route::get('bjr', function () {
    return view('test')->withTitle('Projet Laravel -> Emir');
});

Route::get('structures', function () {
    $data = [
        'number' => '5',
        ];
    return view('structures', $data);
});

Route::get('/', function () {
    return view('welcome');
});

//Registrations
Route::get('register', [RegisterController::class, 'index'])->name('register');
Route::post('register', [RegisterController::class, 'register'])->name('post.register');

//Access login
Route::get('login', [LoginController::class, 'index'])->name('login');
Route::post('login', [LoginController::class, 'login'])->name('post.login');

//Forgetting password and reseting password
Route::get('forgot', [ForgotController::class, 'index'])->name('forgot');
Route::post('forgot', [ForgotController::class, 'store'])->name('post.forgot');

Route::get('logout', [LogoutController::class, 'logout'])->name('logout');
Route::get('reset/{token}', [ResetController::class, 'index'])->name('reset');
Route::post('reset', [ResetController::class, 'reset'])->name('post.reset');

