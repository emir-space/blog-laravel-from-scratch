<?php

namespace App\Http\Controllers;
use App\Models\User;
use App\Notifications\PasswordResetNotification;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Notifications\Notifiable;

class ForgotController extends Controller
{
    public function index() {
        $data = [
            'title' => $description = 'Oublie de mot de passe - '.config('app.name'),
            'description' => $description,
        ];

        return view('auth.forgot', $data);
    }
    public function store() {  //Need to check the availability of infos before inserting in DB
        request()->validate([
            'email'=>'required|email|exists:users',
        ]);

        $token = Str::uuid(); // Used to generate TOKEN
        DB::table('password_resets')->insert([
            'email'=>request('email'),
            'token'=>$token,
            'created_at'=>now(),
        ]);

        $user = User::whereEmail(request('email'))->firstOrFail();
        $user->notify(new PasswordResetNotification($token));

        $success = 'Verifiez votre boite mail et suivez les instructions';
        return back()->withSuccess($success);
    }
}
