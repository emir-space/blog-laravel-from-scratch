<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Validator;

class LoginController extends Controller
{
    public function index() {
        $data = [
            'title' => 'Connexion - '.config('app.name'),
            'description' => 'Connexion à votre compte',
        ];
        return view('auth.login',$data);
    }

    public function login(request $request) {
        request()->validate([
            'email'=>'required|email',
            'password'=>'required|between:9,20',
        ]);

        $remember = request()->has('remember');

        if (Auth::attempt(['email' => request('email'), 'password' => request('password')], $remember)) {
            return redirect('/');
        }
        return back()->withError('Mauvais identifiants .');

    }
}
