<?php

namespace App\Http\Controllers;

use App\Models\User;
use Doctrine\DBAL\Schema\Table;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ResetController extends Controller
{
    public function index (string $token) {
        $password_reset = DB::table('password_resets')->where('token', $token)->first();

        abort_if(!$password_reset, 403);

        $data = [
            'title' => $description = 'Réinitialisation de mot de passe  '.config('app.name'),
            'description' => $description,
            'password_resets' => $password_reset,
        ];
        return view('auth.reset', $data);
    }

    public function reset() {
        request()->validate([
            'email'=>'required|email',
            'token'=>'required',
            'password'=>'required|confirmed|between:9,20',
        ]);

        if(!DB::table('password_resets')
        ->where('email',request('email'))
        ->where('token',request('token'))
        ->count()) {
            $error = 'Vérifier l\'adresse mail saisie .';
            return back()->withError($error)->withInput();
        }

        $user = User::whereEmail(request('email'))->firstOrFail();
        $user->password = bcrypt(request('password'));
        $user->save();

        DB::table('password_resets')->where('email',request('email'))->delete();

        $success = 'Mot de passe mis à jour !';
        return redirect()->route('login')->withSuccess($success);
    }
}
