<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Notifiable;

class PasswordResetNotification extends Notification
{
    use Queueable;
    protected string $token;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(string $token)
    {
        $this->token = $token;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable) //Sending mail ( using mailtrap in my case)
    {
        return (new MailMessage)
                    ->greeting('Bonjour '.$notifiable->name)
                    ->subject('Oublie de mot de passe')
                    ->line('Cliquez sur le lien ci dessous ')
                    ->action('Renitialisez ', url('reset', $this->token))
                    ->salutation('A très bientôt sur notre site.'.config('app.name'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable) //This is needed if we should send notification on database
    {
        return [
            //
        ];
    }
}
