<!DOCTYPE html>
<html lang="fr">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="{{ $description ?? '' }}">

    <title>{{ $title ?? '' }}</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
        integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

    <link rel="stylesheet" href="{{ asset('css/style.css') }}">

</head>

<body>

    <!-- Navigation -->
    @include('layouts.main')

    <!-- Page Content -->
    <div class="container">
        <div class="mt-5 mb-5 pt-5 pb-5">
            @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
            <form action="{{ route('post.register') }}" method="post">
                @csrf
                <div class="form-group">
                    <label for="name">Nom</label>
                    <input type="text" name="name" class="form-control" placeholder="Entez votre nom"
                        value={{ old('name') }}>
                    @error('name')
                        <div class="alert alert-danger"> {{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="email">Addresse Email</label>
                    <input type="email" name="email" class="form-control" aria-describedby="emailHelp"
                        placeholder="Entrez votre email" value={{ old('email') }}>
                    @error('email')
                        <div class="alert alert-danger"> {{ $message }}</div>
                    @enderror
                </div>
                {{-- <div class="form-group">
                <label for="exampleInputEmail1">Addresse Email</label>
                <input type="email" class="form-control" aria-describedby="emailHelp" placeholder="Enter email">
              </div> --}}
                <div class="form-group">
                    <label for="password">Mot de passe</label>
                    <input type="password" name="password" class="form-control"
                        placeholder="Un mot de passe original SVP !">
                    @error('password')
                        <div class="alert alert-danger"> {{ $message }}</div>
                    @enderror
                </div>
                {{-- <div class="form-check">
              <input type="checkbox" class="form-check-input" id="exampleCheck1">
              <label class="form-check-label" for="exampleCheck1">Cochez moi</label>
            </div> --}}
                <button type="submit" class="btn btn-primary">Inscription</button>
            </form>
        </div>

        @yield('content')

    </div>

    @include('layouts.footer')

</body>

</html>
