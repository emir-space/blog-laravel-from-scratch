<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="{{ $description ?? '' }}">

    <title>{{ $title ?? '' }}</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
        integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
</head>

<body>

    @extends('layouts.main')

    @section('content')

        <div class="row">

            {{-- <div class="mt-5 mb-5 pt-5 pb-5">
                @include('includes.sidebar')
            </div> --}}

            <div class="mt-5 mb-5 pt-5 pb-5">

                {{-- début du post --}}
                @foreach ($articles as $article)
                    <div class="card mt-4">
                        <div class="card-body">
                            <h2 class="card-title"><a
                                    href="{{ route('articles.show', ['article' => $article->id]) }}">{{ $article->title }}</a>
                            </h2>
                            <p class="card-text">{{ Str::limit($article->content, 60) }}</p>
                            <span class="auhtor">Par <a href="{{ route('user.profile', ['user' => $article->user->id])}}">{{ $article->user->name }}</a></span>
                            <span class="time">{{ $article->created_at->diffForHumans() }}</span>
                        </div>
                    </div>
                @endforeach

                <div class="Pagination mt-4">
                    {{ $articles->links() }}
                </div>

            </div>
        </div>
    </body>

    </html>
@stop
