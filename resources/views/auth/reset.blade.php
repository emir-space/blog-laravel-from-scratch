<!DOCTYPE html>
<html lang="fr">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="{{ $description ?? '' }}">

    <title>{{ $title ?? '' }}</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
        integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">

</head>

<body>

    @include('layouts.main')
    <!-- Page Content -->
    <div class="container">
        <div class="mt-5 mb-5 pt-5 pb-5">
            @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif

            @if (session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
            @endif
                <div class="card-header">
                Réinitialisation du mot de passe .
                </div>
            <form  action="{{ route('post.reset') }}" method="post">
                @csrf

                <input type="hidden" name="token" value="{{ $password_resets->token }}">
                <div class="form-group">
                    <label for="email">Addresse Email</label>
                    <input type="email" name="email" class="form-control" aria-describedby="emailHelp"
                        placeholder="Entrez votre email" value={{ old('email') }}>
                    @error('email')
                        <div class="alert alert-danger"> {{ $message }}</div>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="password">Nouveau mot de passe</label>
                    <input type="password" name="password" class="form-control">
                    @error('password')
                        <div class="alert alert-danger"> {{ $message }}</div>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="password">Confirmez le mot de passe</label>
                    <input type="password" name="password_confirmation" class="form-control">
                </div>

                <button type="submit" class="btn btn-primary">Se connecter</button>
            </form>
        </div>

        @yield('content')

    </div>

    @include('layouts.footer')

</body>

</html>
