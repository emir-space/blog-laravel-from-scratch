<!DOCTYPE html>
<html lang="fr">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="{{ $description ?? '' }}">

    <title>{{ $title ?? '' }}</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
        integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

    <link rel="stylesheet" href="{{ asset('css/style.css') }}">

</head>

<body>

    @include('layouts.main')
    <!-- Page Content -->
    <div class="container">
        <div class="mt-5 mb-5 pt-5 pb-5">
            @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif

            @if (session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
            @endif

            <form action="{{ route('post.login') }}" method="post">
                @csrf

                <div class="form-group">
                    <label for="email">Addresse Email</label>
                    <input type="email" name="email" class="form-control" aria-describedby="emailHelp"
                        placeholder="Entrez votre email" value={{ old('email') }}>
                    @error('email')
                        <div class="alert alert-danger"> {{ $message }}</div>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="password">Mot de passe</label>
                    <input type="password" name="password" class="form-control"
                        placeholder="Un mot de passe original SVP !">
                    @error('password')
                        <div class="alert alert-danger"> {{ $message }}</div>
                    @enderror
                </div>
                <div class="form-check">
                    <input type="checkbox" class="form-check-input" id="remember" name="remember" value="1">
                    <label class="form-check-label" for="remember">Cochez moi</label>
                </div>
                <button type="submit" class="btn btn-primary">Se conneter</button>
            </form>
        </div>
        <p> <a href="{{ route('forgot') }}">  Mot de pass oublié ?</a></p>
        <p> <a href="{{ route('register') }}">  je n'ai pas de compte </a></p>

        @yield('content')

    </div>

    @include('layouts.footer')

</body>

</html>
