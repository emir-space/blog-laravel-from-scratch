<?php

namespace Database\Factories;

use App\Models\User;
use App\Models\Article;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Article>
 */
class ArticleFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    protected $model = Article::class;
    public function definition()
    {
            return [
                'user_id' => User::factory(),
                'title' => $title = $this->faker->sentence(),
                'slug' => Str::slug($title),
                'content' => $this->faker->paragraph,
            ];
    }
}
